package server

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"net/rpc"

	"github.com/Decker108/amaranthland/src/dataformat"
	"github.com/Decker108/amaranthland/src/model"
)

var messages []*dataformat.ChatInput

type RpcServer struct{}

func (c *RpcServer) SendMsg(input *dataformat.ChatInput, output *dataformat.ChatOutput) error {
	messages = append(messages, input)
	output.Msg = input.Msg
	output.Timestamp = input.Timestamp
	log.Println("Got message:", input, "msg buffer len:", len(messages))
	return nil
}

func (c *RpcServer) GetMsgs(input *dataformat.ChatInput, output *dataformat.ChatOutput) error {
	if len(messages) > 0 {
		for _, msg := range messages {
			if msg.Timestamp > input.Timestamp {
				output.Msg += fmt.Sprintf("%s\n", msg.Msg)
				output.Timestamp = msg.Timestamp
			}
		}
	}
	return nil
}

func (c *RpcServer) MovePlayer(input *dataformat.MovePlayerInput, output *dataformat.EmptyOutput) error {
	for i, _ := range players {
		if players[i].Id == input.PlayerId {
			fmt.Println("Player", i, "moving from", players[i].X, ",", players[i].Y, "to", input.X, ",", input.Y)
			players[i].X = float64(input.X)
			players[i].Y = float64(input.Y)
			players[i].Dir = float64(input.Dir)
		}
	}
	return nil
}

func (c *RpcServer) GetGameState(input *dataformat.GetGameStateInput, output *dataformat.GetGameStateOutput) error {
	output.Players = players
	return nil
}

func (c *RpcServer) RegisterPlayer(input *dataformat.RegisterPlayerInput, output *dataformat.RegisterPlayerOutput) error {
	output.PlayerId = input.PlayerId
	players = append(players, model.NewPlayer(input.PlayerId))
	log.Println("Registered player with ID:", input.PlayerId)
	return nil
}

func (c *RpcServer) RegisterObserver(input *dataformat.RegisterPlayerInput, output *dataformat.EmptyOutput) error {
	log.Println("Registered observer with IP:", input.PlayerId)
	return nil
}

func RunServer() {
	rpcServer := new(RpcServer)
	rpc.Register(rpcServer)
	rpc.HandleHTTP()
	listener, e := net.Listen("tcp", ":1234")
	if e != nil {
		log.Fatal("listen error:", e)
	}
	log.Println("Starting server...")
	go http.Serve(listener, nil)
}
