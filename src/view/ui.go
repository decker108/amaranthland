package view

import (
	"container/list"
	"errors"
	"fmt"
	"log"
	"math"
	"path/filepath"
	"runtime"

	"code.google.com/p/go-uuid/uuid"

	"github.com/Decker108/amaranthland/src/client"
	"github.com/Decker108/amaranthland/src/model"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/sdl_image"
	"github.com/veandco/go-sdl2/sdl_ttf"
)

var window *sdl.Window
var ever bool = true
var windowWidth, windowHeight int32 = 1024, 768
var frames uint32 = 0
var moveBuf *list.List

type Assets struct {
	playerTex *sdl.Texture
	font      *ttf.Font
}

var assets Assets

func goFatal(err error) {
	_, file, line, _ := runtime.Caller(1)
	fname := filepath.Base(file)
	log.Fatalf("Fatal error at %s:%d: %s\n", fname, line, err)
}

func input() {
	for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
		switch t := event.(type) {
		case *sdl.QuitEvent:
			ever = false
		case *sdl.KeyUpEvent:
			switch t.Keysym.Sym {
			case sdl.K_ESCAPE:
				ever = false
			case sdl.K_RETURN:
				break
			}
		}
	}

	left, right, up, down := false, false, false, false
	keystate := sdl.GetKeyboardState()
	if keystate[sdl.SCANCODE_W] != 0 || keystate[sdl.SCANCODE_UP] != 0 {
		up = true
	}
	if keystate[sdl.SCANCODE_S] != 0 || keystate[sdl.SCANCODE_DOWN] != 0 {
		down = true
	}
	if keystate[sdl.SCANCODE_A] != 0 || keystate[sdl.SCANCODE_LEFT] != 0 {
		left = true
	}
	if keystate[sdl.SCANCODE_D] != 0 || keystate[sdl.SCANCODE_RIGHT] != 0 {
		right = true
	}
	if left || right || up || down {
		movEvt := model.Player.Move(left, right, up, down)
		if movEvt != nil {
			moveBuf.PushBack(movEvt)
		}
	}
}

func update() {
	mx, my, _ := sdl.GetMouseState()
	playerCenter := sdl.Point{int32(model.Player.X + model.Player.W/2), int32(model.Player.Y + model.Player.H/2)}
	model.Player.Dir = getDirFromPoints(&playerCenter, &sdl.Point{int32(mx), int32(my)})

	for moveBuf.Len() > 0 {
		elem := moveBuf.Front()
		event := elem.Value.(*model.MoveEvent)
		client.MovePlayer(model.Player.Id, event.X, event.Y, event.Dir)
		moveBuf.Remove(elem)
	}

	frames++
	if math.Mod(float64(frames), 4) == 0 {
		//TODO move to goroutine if too slow for main loop
		gameState := client.GetGameState(model.Player.Id) // xnästa = timeperframe/updateinterval * xlokal + (1-timeperframe/updateinterval) * xserver; 
		//TODO håll både lokalt state och serverstate för player i minnet och interpolera viktat på tid så att lokala kopian mer och mer tar serverns form...upprepa när ett nytt serverstate upptäcks
		model.UpdateGameState(gameState.Players)
	} else {
		//TODO update using extrapolated positions of other players
		model.UpdateGameState(gameState.Players)
	}
}

func render(renderer *sdl.Renderer) {
	renderer.Clear()

	renderer.SetDrawColor(0, 230, 0, 255)
	renderer.FillRect(&sdl.Rect{0, 0, windowWidth, windowHeight})

	dst := sdl.Rect{int32(model.Player.X), int32(model.Player.Y), int32(model.Player.W), int32(model.Player.H)}
	renderer.CopyEx(assets.playerTex, nil, &dst, model.Player.Dir, nil, sdl.FLIP_NONE)

	for i, _ := range model.OtherPlayers {
		//		log.Println("Drawing other players")
		op := model.OtherPlayers[i]
		dst2 := sdl.Rect{int32(op.X), int32(op.Y), int32(op.W), int32(op.H)}
		renderer.CopyEx(assets.playerTex, nil, &dst2, op.Dir, nil, sdl.FLIP_NONE)
	}

	mx, my, _ := sdl.GetMouseState()
	tex, texW, texH := strToTex(fmt.Sprintf("(%d,%d) dir: %f", mx, my, model.Player.Dir), renderer)
	dst3 := sdl.Rect{0, 0, texW, texH}
	renderer.Copy(tex, nil, &dst3)

	renderer.Present()

	sdl.Delay(1000 / 40)
}

func getDirFromPoints(src *sdl.Point, dst *sdl.Point) float64 {
	dx := float64(dst.X - src.X)
	dy := float64(dst.Y - src.Y)
	rads := math.Atan2(-dy, dx)
	return -(rads * 180) / math.Pi
}

func strToTex(textString string, renderer *sdl.Renderer) (*sdl.Texture, int32, int32) {
	color := sdl.Color{R: 255, G: 255, B: 255, A: 0}
	surface := assets.font.RenderText_Solid(textString, color)
	texture, err := renderer.CreateTextureFromSurface(surface)
	if err != nil {
		goFatal(err)
	}
	surface.Free()
	_, _, w, h, _ := texture.Query()
	return texture, int32(w), int32(h)
}

func MainLoop(ip, port string) {
	sdl.Init(sdl.INIT_EVERYTHING)
	runtime.LockOSThread()

	client.SetupConnInfo(ip, port)
	playerId := client.RegisterPlayer(uuid.New())

	if ttf.Init() != 0 {
		goFatal(errors.New("Error initializing TTF"))
	}

	gameTitle := "Amaranthland"
	potentialWindow, err := sdl.CreateWindow(gameTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		int(windowWidth), int(windowHeight), sdl.WINDOW_SHOWN)
	if err != nil {
		goFatal(err)
	}
	window = potentialWindow
	defer window.Destroy()

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		goFatal(err)
	}

	playerImgPath := "assets/player.png"
	surface, err := img.Load(playerImgPath)
	if err != nil {
		goFatal(err)
	}
	texture, err := renderer.CreateTextureFromSurface(surface)
	if err != nil {
		goFatal(err)
	}
	surface.Free()
	assets.playerTex = texture

	fontImgPath := "assets/Consolas.ttf"
	consolasFont, err := ttf.OpenFont(fontImgPath, 38)
	if err != nil {
		goFatal(err)
	}
	assets.font = consolasFont

	model.Player = model.NewPlayer(playerId)

	moveBuf = list.New()

	for ever {
		input()
		update()
		render(renderer)
	}

	sdl.Quit()
}

func ObserverLoop() {
	//TODO use this to give server a GUI, maybe
}
