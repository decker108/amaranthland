package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/Decker108/amaranthland/src/server"
	"github.com/Decker108/amaranthland/src/view"
)

func main() {
	isServer, isClient := false, false
	var serverIp, serverPort string = "localhost", "1234"
	if len(os.Args) > 1 {
		args := os.Args[1:]
		if args[0] == "-runclient" {
			isClient = true
			if len(args) > 1 {
				splitStr := strings.Split(args[1], ":")
				serverIp = splitStr[0]
				if len(splitStr) > 1 {
					serverPort = splitStr[1]
				}
			}
		}
		if args[0] == "-runserver" {
			isServer = true
		}
	}
	
	if isClient {
		runClientLoop(serverIp, serverPort)
	} else if isServer {
		runServerLoop()
	} else {
		fmt.Println("Usage: -runserver OR -runclient [ip:port]")
	}
}

func runClientLoop(ip, port string) {
	view.MainLoop(ip, port)
}

func runServerLoop() {
	server.RunServer()
	for true {
		var str string
		_, err := fmt.Scanf("%s\n", &str)
		if err != nil {
			fmt.Println("got err signal:", err)
			break
		}
		if str == "quit" {
			fmt.Println("got quit signal, quitting")
			break
		}
	}
}
