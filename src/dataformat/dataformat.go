package dataformat

import "github.com/Decker108/amaranthland/src/model"

const (
	SendMsg        string = "RpcServer.SendMsg"
	GetMsgs        string = "RpcServer.GetMsgs"
	RegisterPlayer string = "RpcServer.RegisterPlayer"
	MovePlayer     string = "RpcServer.MovePlayer"
	GetGameState   string = "RpcServer.GetGameState"
)

type IRpcServer interface {
	SendMsg(input *ChatInput, output *ChatOutput) error
	GetMsgs(input *ChatInput, output *ChatOutput) error
	RegisterPlayer(input *RegisterPlayerInput, output *RegisterPlayerOutput) error
	RegisterObserver(input *RegisterPlayerInput, output *EmptyOutput) error
	MovePlayer(input *MovePlayerInput, output *EmptyOutput) error
	GetGameState(input *GetGameStateInput, output *GetGameStateOutput) error
}

type EmptyOutput struct {
	Empty bool
}

type ChatInput struct {
	Msg       string
	Timestamp int64
}

type ChatOutput struct {
	Msg       string
	Timestamp int64
}

type MovePlayerInput struct {
	PlayerId string
	X, Y     int
	Dir      float32
}

type GetGameStateInput struct {
	PlayerId string
}

type GetGameStateOutput struct {
	Players []model.PlayerModel
	//enemies
	//projectiles
	//powerups
}

type RegisterPlayerInput struct {
	PlayerId string
}

type RegisterPlayerOutput struct {
	PlayerId string
}
