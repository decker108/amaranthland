package model

import (
	"log"
	"math"
)

var windowWidth, windowHeight int32 = 1024, 768
var OtherPlayers []PlayerModel
var Player PlayerModel

type PlayerModel struct { //TODO split into interface and impl
	Id       string
	X, Y     float64
	W, H     float64
	Dir      float64
	Velocity float64
}

func NewPlayer(id string) PlayerModel {
	return PlayerModel{
		Id:       id,
		X:        float64(windowWidth) / 2.0,
		Y:        float64(windowHeight) / 2.0,
		W:        48,
		H:        48,
		Dir:      0,
		Velocity: 9,
	}
}

func NewPlayerAtPos(id string, x, y, dir float64) PlayerModel {
	player := NewPlayer(id)
	player.X = x
	player.X = y
	player.Dir = dir
	return player
}

func (e *PlayerModel) Move(left, right, up, down bool) *MoveEvent {
	if e.Y-e.Velocity < -10 {
		up = false
	}
	if e.Y+e.Velocity > float64(windowHeight)-e.H {
		down = false
	}
	if e.X-e.Velocity < 0 {
		left = false
	}
	if e.X+e.Velocity > float64(windowWidth)-e.W {
		right = false
	}

	dx, dy := 0.0, 0.0
	v_xy := float64(e.Velocity)
	if (left || right) && (up || down) {
		if right {
			dx = v_xy / math.Sqrt(2)
		}
		if left {
			dx = dx + (1-v_xy)/math.Sqrt(2)
		}
		if up {
			dy = dy + (1-v_xy)/math.Sqrt(2)
		}
		if down {
			dy = v_xy / math.Sqrt(2)
		}
	} else {
		if right {
			dx = v_xy
		}
		if left {
			dx = dx + (1 - v_xy)
		}
		if up {
			dy = dy + (1 - v_xy)
		}
		if down {
			dy = v_xy
		}
	}

	e.X += dx
	e.Y += dy

	if dx != 0 || dy != 0 {
		return &MoveEvent{e.X, e.Y, e.Dir, e.Velocity, false}
	}
	return nil
}

type MoveEvent struct {
	X, Y, Dir float64
	Velocity float64
	Stopped bool
}

func UpdateGameState(updatedPlayers []PlayerModel) {
	if len(updatedPlayers) > 1 {
		addPlayersToSync(updatedPlayers)
	}

//	log.Printf("%#v", updatedPlayers)

	for _, serverPlayerState := range updatedPlayers {
		if serverPlayerState.Id == Player.Id {
			Player.X = serverPlayerState.X
			Player.Y = serverPlayerState.Y
		} else {
			for i, _ := range OtherPlayers {
				if serverPlayerState.Id == OtherPlayers[i].Id {
					OtherPlayers[i].X = serverPlayerState.X
					OtherPlayers[i].Y = serverPlayerState.Y
					OtherPlayers[i].Dir = serverPlayerState.Dir
				}
			}
		}
	}
}

func addPlayersToSync(playersFromServer []PlayerModel) {
	for _, p := range playersFromServer {
		//skip if it's the local player
		if p.Id == Player.Id {
			continue
		}
		//add if there are no stored remote players and this isn't the local player
		if len(OtherPlayers) == 0 && p.Id != Player.Id {
			log.Println("Added remote player")
			OtherPlayers = append(OtherPlayers, NewPlayer(p.Id))
			continue
		}
		//add if the id cannot be found among the stored remote players
		found := false
		for _, op := range OtherPlayers {
			found = found || op.Id == p.Id
		}
		if !found {
			log.Println("Added remote player")
			OtherPlayers = append(OtherPlayers, NewPlayer(p.Id))
		}
	}
}
