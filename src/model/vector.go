package model

import "math"

type Vector struct {
	X, Y float64
}

func (v Vector) Length() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func Translate(pos, dir Vector) Vector {
	return Vector{
		X: pos.X + dir.X,
		Y: pos.Y + dir.Y,
	}
}

func Add(v1, v2 Vector) Vector {
	return Vector{
		X: v1.X + v2.X,
		Y: v1.Y + v2.Y,
	}
}

func Diff(v1, v2 Vector) Vector {
	return Vector{
		X: v1.X - v2.X,
		Y: v1.Y - v2.Y,
	}
}

func Scale(v Vector, s float64) Vector {
	return Vector{
		X: v.X * s,
		Y: v.Y * s,
	}
}

func Normalized(v Vector) Vector {
	l := v.Length()
	return Vector{
		X: v.X / l,
		Y: v.Y / l,
	}
}
