package client

import (
	"fmt"
	"log"
	"net/rpc"
	"time"

	"github.com/Decker108/amaranthland/src/dataformat"
)

var (
	remoteIp, remotePort  string
	client                *rpc.Client
	timeOfLastSentMsg     int64
	timeOfLastReceivedMsg int64
)

func GetChatMsgs() string {
	client := getClient()
	args := &dataformat.ChatInput{"", timeOfLastSentMsg}
	var reply *dataformat.ChatInput
	err := client.Call(dataformat.GetMsgs, args, &reply)
	if err != nil {
		log.Fatal("Chat.GetMsgs error:", err)
	}

	if reply.Timestamp > timeOfLastReceivedMsg {
		timeOfLastReceivedMsg = reply.Timestamp
		return reply.Msg
	}
	return ""
}

func SendChatMsg(inputStr string) {
	client := getClient()
	timestamp := time.Now().UnixNano()
	input := &dataformat.ChatInput{inputStr, timestamp}
	output := new(dataformat.ChatOutput)
	client.Go(dataformat.SendMsg, input, output, nil)
	timeOfLastSentMsg = timestamp
}

func MovePlayer(playerId string, x, y float64, dir float64) {
	client := getClient()
	input := &dataformat.MovePlayerInput{playerId, int(x), int(y), float32(dir)}
	output := new(dataformat.EmptyOutput)
	client.Go(dataformat.MovePlayer, input, output, nil)
}

func GetGameState(playerId string) *dataformat.GetGameStateOutput {
	client := getClient()
	input := &dataformat.GetGameStateInput{playerId}
	var output *dataformat.GetGameStateOutput
	err := client.Call(dataformat.GetGameState, input, &output)
	if err != nil {
		log.Fatal("GetGameState error:", err)
	}

	return output
}

func RegisterPlayer(ipAddr string) string {
	client := getClient()
	input := &dataformat.RegisterPlayerInput{ipAddr}
	var output *dataformat.RegisterPlayerOutput
	err := client.Call(dataformat.RegisterPlayer, input, &output)
	if err != nil {
		log.Fatal("RegisterPlayer error:", err)
	}

	return output.PlayerId
}

func RegisterObserver() {

}

func SetupConnInfo(ip, port string) {
	remoteIp = ip
	remotePort = port
}

func getClient() *rpc.Client {
	if client == nil {
		client1, err := rpc.DialHTTP("tcp", fmt.Sprintf("%s:%s", remoteIp, remotePort))
		if err != nil {
			log.Fatal("Error connecting to server:", err)
		}
		client = client1
		log.Println("Connecting to", remoteIp, "on port", remotePort)
	}
	return client
}
